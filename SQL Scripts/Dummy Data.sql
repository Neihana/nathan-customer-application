USE [WebShop]
GO
SET IDENTITY_INSERT [Customer] ON 

GO
INSERT [Customer] ([Id], [Name], [Email]) VALUES (1, N'Michael Jordan', N'Michael.Jordan@gmail.com')
GO
INSERT [Customer] ([Id], [Name], [Email]) VALUES (2, N'LeBron James', N'The.King@gmail.com')
GO
INSERT [Customer] ([Id], [Name], [Email]) VALUES (3, N'Stephen Curry', N'Chef.Curry@gmail.com')
GO
INSERT [Customer] ([Id], [Name], [Email]) VALUES (4, N'Kevin Durant', N'KDThunder@gmail.com')
GO
INSERT [Customer] ([Id], [Name], [Email]) VALUES (5, N'Steven Adams', N'TheBigKiwi@gmail.com')
GO
SET IDENTITY_INSERT [Customer] OFF
GO
SET IDENTITY_INSERT [Order] ON 

GO
INSERT [Order] ([Id], [Price], [CreatedDate], [CustomerId]) VALUES (1, 1232, CAST(N'2016-03-02 16:20:00.727' AS DateTime), 1)
GO
INSERT [Order] ([Id], [Price], [CreatedDate], [CustomerId]) VALUES (2, 93847, CAST(N'2016-03-02 16:20:00.730' AS DateTime), 1)
GO
INSERT [Order] ([Id], [Price], [CreatedDate], [CustomerId]) VALUES (3, 2, CAST(N'2016-03-02 16:20:23.180' AS DateTime), 2)
GO
INSERT [Order] ([Id], [Price], [CreatedDate], [CustomerId]) VALUES (4, 20000, CAST(N'2016-03-02 16:20:23.207' AS DateTime), 3)
GO
INSERT [Order] ([Id], [Price], [CreatedDate], [CustomerId]) VALUES (5, 10.5, CAST(N'2016-03-02 16:20:23.210' AS DateTime), 3)
GO
SET IDENTITY_INSERT [Order] OFF
GO

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Webshop.Controllers;
using Webshop.Data_Layer;
using Webshop.Repositories;

namespace Webshop.Tests
{
    [TestClass]
    public class TestWebshopApi
    {
        public readonly ICustomerRepository MockCustomerRepository;

        public TestWebshopApi()
        {
            IEnumerable<Customer> customerList = new List<Customer>
            {
            new Customer { Id = 1, Name = "Richie McCaw", Email = "SirRichie@gmail.com" },
            new Customer { Id = 2, Name = "Daniel Carter", Email = "DC10@gmail.com" },
            new Customer { Id = 3, Name = "Maa Nonu", Email = "NonuNonuNonu@gmail.com" }
            };

            Mock<ICustomerRepository> repositoryMock = new Mock<ICustomerRepository>();

            repositoryMock.Setup(repo => repo.GetCustomers()).Returns(customerList);
            repositoryMock.Setup(repo => repo.GetCustomerById(1)).Returns(new Customer { Id = 1, Name = "Richie McCaw", Email = "SirRichie@gmail.com" });
            this.MockCustomerRepository = repositoryMock.Object;
        }
        [TestMethod]
        public void GetCustomers_ReturnAllCustomers()
        {
            var controller = new CustomersController(this.MockCustomerRepository);

            dynamic result = controller.GetCustomers();
            
            Assert.IsNotNull(result);

            var customers = result.Content;
            Assert.AreEqual(3, customers.Count);
        }

        [TestMethod]
        public void GetCustomerById_ReturnSpecifiedCustomer()
        {
            var controller = new CustomersController(this.MockCustomerRepository);

            var result = controller.GetCustomerById(1);

            var response = result as OkNegotiatedContentResult<Customer>;

            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Content.Id);
            Assert.AreEqual("Richie McCaw", response.Content.Name);
        }

        [TestMethod]
        public void GetCustomerById_CustomerNotFound()
        {
            var controller = new CustomersController(this.MockCustomerRepository);

            IHttpActionResult result = controller.GetCustomerById(999);

            Assert.IsInstanceOfType(result, typeof(NotFoundResult));
        }
    }
}

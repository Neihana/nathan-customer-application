﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Webshop.Data_Layer;
using Webshop.Repositories;

namespace Webshop.Controllers
{
    public class CustomersController : ApiController
    {
        ICustomerRepository _repo;

        public CustomersController(ICustomerRepository repository)
        {
            _repo = repository;
        }

        public IHttpActionResult GetCustomers()
        {
            var customers = _repo.GetCustomers().Select(c => new { c.Id, c.Name, c.Email }).ToList();
            return Ok(customers);
        }

        public IHttpActionResult GetCustomerById(int id)
        {
            Customer returnCustomer = _repo.GetCustomerById(id);

            if (returnCustomer == null)
                return NotFound();
            else
                return Ok(returnCustomer);
        }
    }
}

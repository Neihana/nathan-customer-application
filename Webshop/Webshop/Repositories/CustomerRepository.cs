﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webshop.Data_Layer;

namespace Webshop.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        WebShopEntities db = new WebShopEntities();

        public IEnumerable<Customer> GetCustomers()
        {
            return db.Customers.ToList();
        }

        public Customer GetCustomerById(int id)
        {
            return db.Customers.Include("Orders").SingleOrDefault(c => c.Id == id);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Webshop.Data_Layer;

namespace Webshop.Repositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomerById(int id);
    }
}